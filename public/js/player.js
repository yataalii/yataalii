document.addEventListener('DOMContentLoaded', function () {

    //on sélectionne tous les boutons play
    const playButtons = document.querySelectorAll('.play');
    const stopButtons = document.querySelectorAll('.stop');

    playButtons.forEach((playButton) => {
        playButton.addEventListener('click', clickPlay);
    });

    stopButtons.forEach((stopButton) => {
        stopButton.addEventListener('click', clickResume);
    });
});


//function click play 
function clickPlay() {

    let player = document.querySelector(`#${this.dataset.player}`);
    if (player.paused) {
        console.log('play')
        player.play();
    } else {
        console.log('pause')
        player.pause();
    }

}

//function click resume 
function clickResume() {
    console.log('stop')
    let player = document.querySelector(`#${this.dataset.player}`);

    player.currentTime = 0;
    player.pause();

}

function update(player) {

    let duration = player.duration; // Durée totale
    let time = player.currentTime; // Temps écoulé
    let fraction = time / duration;
    let percent = Math.ceil(fraction * 100);
    let progress = document.querySelector('#progressBar');

    progress.style.width = percent + '%';
    progress.textContent = percent + '%';
}