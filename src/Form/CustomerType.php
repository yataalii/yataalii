<?php

namespace App\Form;

use App\Entity\Customer;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('phone')
            ->add('adressLine1')
            ->add('adressLine2')
            ->add('city')
            ->add('postalCode')
            ->add('country')
            ->add('createdAt')
            ->add('modifiedAt')
            ->add('user', EntityType::class, [
                // looks for choices from this entity
                'class' => User::class,
                // uses the User.email property as the visible option string
                'choice_label' => 'email',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
