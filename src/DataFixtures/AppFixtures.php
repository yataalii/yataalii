<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Customer;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i <= 4; $i++) {
            $customer = new Customer();
            $customer->setPhone("0642786853")
                ->setAdressLine1("8 rue chacundier")
                ->setAdressLine1("")
                ->setCity("Manosque")
                ->setPostalCode("04100")
                ->setCountry("France")
                ->setCreatedAt(new \DateTimeImmutable());

            $manager->persist($customer);
        }
        $manager->flush();
    }
}
