<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class YataaliiController extends AbstractController
{
    /**
     * @Route("/", name="yataalii")
     */
    public function index(ProductRepository $productRepository): Response
    {
        $products = $productRepository->findAll();
        return $this->render('yataalii/boutique.html.twig', [
            'products' => $products,
        ]);
    }
}
