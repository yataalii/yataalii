<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CartController extends AbstractController
{
    /**
     * @Route("/cart", name="cart")
     */
    public function index(SessionInterface $session): Response
    {
        $items = $session->get('cart', []);

        $cartTotal = $this->calculTotalCart($items);

        return $this->render('cart/index.html.twig', [
            'subtotal' => $cartTotal['subtotal'],
            'total' => $cartTotal['total'],
            'items' => $items
        ]);
    }

    /** Retourne le total panier */
    public function calculTotalCart(array $items)
    {
        $cartTotal = ['subtotal' => 0, 'total' => 0];
        // Calcul total Price
        foreach ($items as $item) {
            $cartTotal['subtotal']  += $item['price'] * $item['qte'];

            $cartTotal['total']     += $item['price'] * $item['qte'];
        }
        return $cartTotal;
    }


    /**
     * @Route("/cart/add", name="cart_add", methods="POST")
     */
    public function add(Request $request, SessionInterface $session, ProductRepository $productRepository): Response
    {
        $id = $request->request->get('id', null);
        $qte = $request->request->get('quantity', 0);

        // Si on a pas d'id on redirtige vers la page home
        if ($id == null)
            return $this->redirectToRoute('yataalii');

        $product = $productRepository->find($id);

        $cart = $session->get('cart', []);

        if (isset($cart[$id]) && isset($cart[$id]['qte'])) {

            $cart[$id]['qte'] += $qte;
        } else {

            $cart[$id]['picture'] = $product->getProductPicture();
            $cart[$id]['qte'] = $qte;
            $cart[$id]['name'] = $product->getProductName();
            $cart[$id]['price'] = $product->getProductPrice();
        }

        $session->set('cart', $cart);
        return $this->redirectToRoute('cart');
    }
}
